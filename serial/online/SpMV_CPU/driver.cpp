/*
 *  Copyright 2008-2009 NVIDIA Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


#include <iostream>
#include <stdio.h>
#include "cmdline.h"
#include "gallery.h"
#include "config.h"
#include "timer.h"

#include "tests.h"
#include "format_estimate.h"
#include "run_once_for_estimate.h"

void usage(int argc, char** argv)
{
    std::cout << "Usage:\n";
    std::cout << "\t" << argv[0] << " with following parameters:\n";
    std::cout << "\t" << " my_matrix.mtx\n";
    std::cout << "\t" << " --precision=32(or 64)\n";
    std::cout << "\t" << " --max_diags_limit=20\n";
    std::cout << "\t" << " --dia_fill_ratio=0.95\n";
    std::cout << "\t" << " --min_diags_ratio=0.1\n";
    std::cout << "\t" << " --ell_max_cols=100\n";
    std::cout << "\t" << " --ell_min_deviation=1\n";
    std::cout << "Note: my_matrix.mtx must be real-valued sparse matrix in the MatrixMarket file format.\n"; 
}


template <typename IndexType, typename ValueType>
void run_all_kernels(int argc, char **argv)
{
    char * mm_filename = NULL;
    for(int i = 1; i < argc; i++){
        if(argv[i][0] != '-'){
            mm_filename = argv[i];
            break;
        }
    }

    csr_matrix<IndexType,ValueType> csr;
    
    if (mm_filename == NULL)
    {
        printf("No input MM file!\n");
        return;
    }
    else
        csr = read_csr_matrix<IndexType,ValueType>(mm_filename);
    printf("Using %d-by-%d matrix with %d nonzero values\n", csr.num_rows, csr.num_cols, csr.num_nonzeros); 

#ifdef TEST
//print in CSR format
    printf("Writing matrix in CSR format to test_CSR ...\n");
    FILE *fp = fopen("test_CSR", "w");
    fprintf(fp, "%d\t%d\t%d\n", csr.num_rows, csr.num_cols, csr.num_nonzeros);
    fprintf(fp, "csr.Ap:\n");
    for (IndexType i=0; i<csr.num_rows+1; i++)
    {
      fprintf(fp, "%d\n", csr.Ap[i]);
    }
    fprintf(fp, "csr.Aj:\n");
    for (IndexType i=0; i<csr.num_nonzeros; i++)
    {
      fprintf(fp, "%d\n", csr.Aj[i]);
    }
    fprintf(fp, "csr.Ax:\n");
    for (IndexType i=0; i<csr.num_nonzeros; i++)
    {
      fprintf(fp, "%lf\n", csr.Ax[i]);
    }
    fclose(fp);
#endif 

    // fill matrix with random values: some matrices have extreme values, 
    // which makes correctness testing difficult, especially in single precision
    
    srand(13);
    for(IndexType i = 0; i < csr.num_nonzeros; i++){
        csr.Ax[i] = 1.0 - 2.0 * (rand() / (RAND_MAX + 1.0)); 
    }
    
    printf("\nfile=%s rows=%d cols=%d nonzeros=%d\n", mm_filename, csr.num_rows, csr.num_cols, csr.num_nonzeros);

    //Input the best kernel for each format
    int dia_kernel_tag = 0, ell_kernel_tag = 0, csr_kernel_tag = 0, coo_kernel_tag = 0;
    FILE *fp_tag;
    fp_tag = fopen(KERNEL_TAGS, "r");
    fscanf(fp_tag, "%d", &dia_kernel_tag);
    fscanf(fp_tag, "%d", &ell_kernel_tag);
    fscanf(fp_tag, "%d", &csr_kernel_tag);
    fscanf(fp_tag, "%d", &coo_kernel_tag);
    fclose(fp_tag);

    printf("dia_kernel_tag : %d\n", dia_kernel_tag );
    printf("ell_kernel_tag : %d\n", ell_kernel_tag );
    printf("csr_kernel_tag : %d\n", csr_kernel_tag );
    printf("coo_kernel_tag : %d\n", coo_kernel_tag );
    
    FILE *fp_estimate = fopen(ESTIMATE_INFO, "w");
    fprintf(fp_estimate, "file=%s rows=%d cols=%d nonzeros=%d\n", mm_filename, csr.num_rows, csr.num_cols, csr.num_nonzeros);

    double dia_gflops, ell_gflops, csr_gflops, coo_gflops;
    double estimate_max_gflops = 0, actual_max_gflops = 0;
    char *estimate_best_format, *actual_best_format;
    int actual_tag = 0, estimate_tag_DT = 0;  //csr.tag is estimated

    //Estimate the best format
    timer estimate_time_struct;

    format_estimate ( csr);
//    printf ("tag before run once : %d\n", csr.tag);
    estimate_tag_DT = csr.tag;
    double confidence_tmp[4];
    confidence_tmp[0] = csr.confidence[0];
    confidence_tmp[1] = csr.confidence[1];
    confidence_tmp[2] = csr.confidence[2];
    confidence_tmp[3] = csr.confidence[3];
    printf ("\nDevision tree Confidence :\n");
    printf ("DIA : %f\n", confidence_tmp[0]);
    printf ("ELL : %f\n", confidence_tmp[1]);
    printf ("CSR : %f\n", confidence_tmp[2]);
    printf ("COO : %f\n\n", confidence_tmp[3]);

    if (csr.tag == 0)
      csr.tag = run_once_for_estimate ( csr, dia_kernel_tag, ell_kernel_tag, csr_kernel_tag, coo_kernel_tag );  

    double estimate_time = estimate_time_struct.milliseconds_elapsed();

    double dia_contime = 0, ell_contime = 0, coo_contime = 0;
    double dia_runtime = 0, ell_runtime = 0, coo_runtime = 0;

    if ( csr.tag == 1 )
    {
      estimate_best_format = "DIA";
      test_dia_matrix_kernels(csr, dia_kernel_tag, &dia_gflops, &dia_contime, &dia_runtime);
      estimate_max_gflops = dia_gflops;
    }
    else if ( csr.tag == 2 )
    {
      estimate_best_format = "ELL";
      test_ell_matrix_kernels(csr, ell_kernel_tag, &ell_gflops, &ell_contime, &ell_runtime);
      estimate_max_gflops = ell_gflops;
    }
    else if ( csr.tag == 3 )
    {
      estimate_best_format = "CSR";
      test_csr_matrix_kernels(csr, csr_kernel_tag, &csr_gflops);
      estimate_max_gflops = csr_gflops;
    }
    else if ( csr.tag == 4 )
    {
      estimate_best_format = "COO";
      test_coo_matrix_kernels(csr, coo_kernel_tag, &coo_gflops, &coo_contime, &coo_runtime);
      estimate_max_gflops = coo_gflops;
    }

    //Run all the formats to find the actual best format
    printf ("\nTest all the four formats performance:\n");
    test_dia_matrix_kernels(csr, dia_kernel_tag, &dia_gflops, &dia_contime, &dia_runtime);
      fflush(stdout);
    test_ell_matrix_kernels(csr, ell_kernel_tag, &ell_gflops, &ell_contime, &ell_runtime);
      fflush(stdout);
    test_csr_matrix_kernels(csr, csr_kernel_tag, &csr_gflops);
      fflush(stdout);
    test_coo_matrix_kernels(csr, coo_kernel_tag, &coo_gflops, &coo_contime, &coo_runtime);
      fflush(stdout);


    double all_contime = dia_contime + ell_contime + coo_contime;
    double all_con_times = all_contime / csr.time;
    printf ("\n\n DIA conversion time : %f ms\n", dia_contime);
    printf (" ELL conversion time : %f ms\n", ell_contime);
    printf (" COO conversion time : %f ms\n", coo_contime);
    printf (" Format conversion time : %f ms, %.2f times of CSR-SpMV\n", all_contime, all_con_times);

    double brute_force_times = all_con_times + dia_runtime/csr.time + ell_runtime/csr.time + 1 + coo_runtime/csr.time;
    printf ("\n\nBrute-Force time : %.2f times of CSR-SpMV\n\n", brute_force_times);

    if (actual_max_gflops < dia_gflops)
    {
     actual_max_gflops = dia_gflops;
     actual_best_format = "DIA";
     actual_tag = 1;
    }
    if (actual_max_gflops < ell_gflops)
    {
     actual_max_gflops = ell_gflops;
     actual_best_format = "ELL";
     actual_tag = 2;
    }
    if (actual_max_gflops < csr_gflops)
    {
     actual_max_gflops = csr_gflops;
     actual_best_format = "CSR";
     actual_tag = 3;
    }
    if (actual_max_gflops < coo_gflops)
    {
     actual_max_gflops = coo_gflops;
     actual_best_format = "COO";
     actual_tag = 4;
    }

    //Unify the best performance in the same format
    if ( csr.tag == 1 && estimate_max_gflops != dia_gflops )
    {
      estimate_max_gflops = dia_gflops;
    }
    if ( csr.tag == 2 && estimate_max_gflops != ell_gflops )
    {
      estimate_max_gflops = ell_gflops;
    }
    if ( csr.tag == 3 && estimate_max_gflops != csr_gflops )
    {
      estimate_max_gflops = csr_gflops;
    }
    if ( csr.tag == 4 && estimate_max_gflops != coo_gflops )
    {
      estimate_max_gflops = coo_gflops;
    }

    if ( actual_tag != estimate_tag_DT )
    {
      printf (" ++++++++++ Decision tree : Wrong Predict!\n");
      fprintf (fp_estimate, "Decision tree : Wrong Predict!\n");
    }
    else
    {
      printf (" ++++++++++ Decision tree : Right Predict!\n");
      fprintf (fp_estimate, "Decision tree : Right Predict!\n");
    }

    double perf_lost = (actual_max_gflops - estimate_max_gflops) / actual_max_gflops;
    if ( strcmp (actual_best_format, estimate_best_format) )
    {
      printf (" ++++++++++ SMAT : Wrong Predict! Performance lost : %.2f %%\n", 100*perf_lost);
      fprintf (fp_estimate, "SMAT : Wrong Predict! Performance lost : %.2f %%\n", 100*perf_lost);
    }
    else
    {
      printf (" ++++++++++ SMAT : Right Predict!\n");
      fprintf (fp_estimate, "SMAT : Right Predict!\n");
    }
    
    double times_of_CSR = estimate_time / csr.time;
    printf ("\n ++++++++++ Estimate time: %.4lf ms, %.2lf times of CSR-SpMV\n", estimate_time, times_of_CSR);
    fprintf (fp_estimate, "Estimate time : %.4lf ms, %.2lf times of CSR-SpMV\n", estimate_time, times_of_CSR);
    printf ("\n ++++++++++ Estimate best format : %s , Performance : %.2f GFLOPS\n", estimate_best_format, estimate_max_gflops);
    fprintf (fp_estimate, "Estimate best format : %s , Performance : %.2f GFLOPS\n", estimate_best_format, estimate_max_gflops);
    printf ("\n ++++++++++ CSR Performance : %.2f GFLOPS\n", csr_gflops);
    fprintf (fp_estimate, "CSR Performance : %.2f GFLOPS\n", csr_gflops);
    printf ("\n ++++++++++ Actual best format : %s , Performance : %.2f GFLOPS\n", actual_best_format, actual_max_gflops);
    fprintf (fp_estimate, "Actual best format : %s , Performance : %.2f GFLOPS\n", actual_best_format, actual_max_gflops);
    fprintf (fp_estimate, "Brute-Force time : %.2f times of CSR-SpMV\n\n", brute_force_times);
    fflush(stdout);

    fclose(fp_estimate);

    delete_host_matrix(csr);
}

int main(int argc, char** argv)
{
    if (get_arg(argc, argv, "help") != NULL){
        usage(argc, argv);
        return EXIT_SUCCESS;
    }

    int precision = 32;
    char * precision_str = get_argval(argc, argv, "precision");
    if(precision_str != NULL)
        precision = atoi(precision_str);
    printf("Using %d-bit floating point precision\n", precision);

    if(precision ==  32)
        run_all_kernels<int, float>(argc,argv);
    else if(precision == 64)
        run_all_kernels<int, double>(argc,argv);
    else{
        usage(argc, argv);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

