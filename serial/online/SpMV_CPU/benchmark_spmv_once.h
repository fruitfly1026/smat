/*
 *  Copyright 2008-2009 NVIDIA Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */



#pragma once
#include <stdio.h>
#include "sparse_formats.h"
#include "config.h"
#include "timer.h"

#include "benchmark_spmv.h"
   
template <typename SparseMatrix, typename SpMV>
double benchmark_spmv_once(SparseMatrix & sp_host, SpMV spmv, const char * method_name)
{

    typedef typename SparseMatrix::value_type ValueType;
    typedef typename SparseMatrix::index_type IndexType;

    //initialize host arrays
    ValueType * x_host = new_array<ValueType>(sp_host.num_cols);
    ValueType * y_host = new_array<ValueType>(sp_host.num_rows);

    for(IndexType i = 0; i < sp_host.num_cols; i++)
        x_host[i] = rand() / (RAND_MAX + 1.0); 
    std::fill(y_host, y_host + sp_host.num_rows, 0);

    // warmup    
    timer time_one_iteration;
    spmv(sp_host, x_host, y_host);
    double estimated_time = time_one_iteration.seconds_elapsed();

    //printf("estimated time %f\n", (float) estimated_time);

    // time only one SpMV iterations
    timer t;
    spmv(sp_host, x_host, y_host);
    double msec_per_iteration = t.milliseconds_elapsed();
    double sec_per_iteration = msec_per_iteration / 1000.0;
    double GFLOPs = (sec_per_iteration == 0) ? 0 : (2.0 * (double) sp_host.num_nonzeros / sec_per_iteration) / 1e9;
    double GBYTEs = (sec_per_iteration == 0) ? 0 : ((double) bytes_per_spmv(sp_host) / sec_per_iteration) / 1e9;
    sp_host.gflops = GFLOPs;
    sp_host.time = msec_per_iteration;
    const char * location = "cpu" ;
    printf("\tbenchmarking %-20s [%s]: %8.4f ms ( %5.2f GFLOP/s %5.1f GB/s)\n", \
            method_name, location, msec_per_iteration, GFLOPs, GBYTEs); 

    //deallocate buffers
    delete_array(x_host);
    delete_array(y_host);

    return msec_per_iteration;
}

template <typename SparseMatrix, typename SpMV>
double benchmark_spmv_on_host_once(SparseMatrix & sp_host, SpMV spmv, char * method_name = NULL)
{
    return benchmark_spmv_once<SparseMatrix, SpMV>(sp_host, spmv, method_name);
}

