#!/usr/bin/perl

#$matrices = "../../../mats_list/test_one_mat";
$matrices = "../../../mats_list/mats_ncomplex_nsmall";
$perf_path = "../Mats_features";
$out_best = ">best_perf";
$out_DIA = ">DIA_perf";
$out_ELL = ">ELL_perf";
$out_CSR = ">CSR_perf";
$out_COO = ">COO_perf";

$count = 0;

print "Using $matrices input MM matrices\n";
open(OUT_BEST, $out_best) || die "Cannot open $out_best : $!\n";
open(OUT_DIA, $out_DIA) || die "Cannot open $out_DIA : $!\n";
open(OUT_ELL, $out_ELL) || die "Cannot open $out_ELL : $!\n";
open(OUT_CSR, $out_CSR) || die "Cannot open $out_CSR : $!\n";
open(OUT_COO, $out_COO) || die "Cannot open $out_COO : $!\n";

open (MM_MATS, $matrices) || die "Cannot open $matrices : $!\n";
while ($mm_file = <MM_MATS>)
{
    chomp $mm_file;
    @name = split ( /\//, $mm_file);  
    @tmp = split (/.mtx/, $name[-1]);

    $perf_file = $perf_path . "\/$name[5]\/$name[6]\/$tmp[0]_perf" . "\/mat_features";
    open (PERF_FILE, $perf_file) || die "Cannot open $perf_file : $!\n";
    while ($line = <PERF_FILE>)
    {
        chomp $line;
        @words = split (/ /, $line);  
        if ($words[0] eq "DIA" )
        {
          $DIA_perf = $words[-1];
          printf OUT_DIA ("%f\n", $words[-1]);
        }
        if ($words[0] eq "ELL" )
        {
          $ELL_perf = $words[-1];
          printf OUT_ELL ("%f\n", $words[-1]);
        }
        if ($words[0] eq "CSR" )
        {
          $CSR_perf = $words[-1];
          printf OUT_CSR ("%f\n", $words[-1]);
        }
        if ($words[0] eq "COO" )
        {
          $COO_perf = $words[-1];
          printf OUT_COO ("%f\n", $words[-1]);
        }
        if ($words[0] eq "Best_format" )
        {
          $best_format = $words[-1];
        }
    }
    close(PERF_FILE);
    if ( $best_format eq "DIA")
    {
      printf OUT_BEST ("%f\n", $DIA_perf);
    }
    elsif ( $best_format eq "ELL")
    {
      printf OUT_BEST ("%f\n", $ELL_perf);
    }
    elsif ( $best_format eq "CSR")
    {
      printf OUT_BEST ("%f\n", $CSR_perf);
    }
    elsif ( $best_format eq "COO")
    {
      printf OUT_BEST ("%f\n", $COO_perf);
    }
}
close(MM_MATS);
close(OUT_BEST);
close(OUT_DIA);
close(OUT_ELL);
close(OUT_CSR);
close(OUT_COO);
printf ("Output best performance to $out_best\n");
printf ("Output best performance to $out_DIA\n");
printf ("Output best performance to $out_ELL\n");
printf ("Output best performance to $out_CSR\n");
printf ("Output best performance to $out_COO\n");
