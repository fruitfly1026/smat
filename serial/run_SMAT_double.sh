#!/bin/bash

# Notice the path in the perl files.

printf "## offline process ##\n";

printf "## data mining ##\n";
printf "### Extract matrix features ###\n";
cd ./offline/SpMV_CPU; pwd;
./run_double.pl
printf "### Extract data file from learning matrix set ###\n";
cd ../data_mining; pwd;
./gen_data_learn_double.pl
printf "### Extract test file from testing matrix set ###\n";
./gen_data_test_double.pl
printf "### Running C5.0 ###\n";
./run_C50_double.pl

printf "\n## online process ##\n";
cd ../../online/SpMV_CPU;
pwd
printf "### Generate rules separately ###\n";
./gen_rules_double.pl
printf "### Generate format_estimate.h ###\n";
./gen_format_estimate_double.pl
printf "### Online run SpMV ###\n";
./run_double.pl

printf "## Record Performance ##\n";
# Compare with CSR
# Compare to MKL

# Accuracy test
# With the best format
# decision tree accuray

# Timing 
# search time
